package com.hjh6.mp.multi.source.controller;

import com.hjh6.mp.multi.source.entity.Dept;
import com.hjh6.mp.multi.source.entity.Menu;
import com.hjh6.mp.multi.source.service.IDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    private IDeptService deptService;

    @RequestMapping("/test")
    public String test(){
        return "demo|test";
    }


    @RequestMapping("/test1")
    public String test1(){
        List<Dept> rowList = deptService.list();
        return "demo|test|size:"+(rowList == null ? 0: rowList.size());
    }


    @RequestMapping("/test2")
    public String test2(){
        List<Dept> rowList = deptService.list();
        int rowSize = rowList == null ? 0: rowList.size();

        List<Menu> menuList = deptService.findMenu();
        int menuSize = menuList == null ? 0:menuList.size();

        return "demo|test|size:"+rowSize +"|menuSize:"+menuSize;
    }

}
