package com.hjh6.mp.multi.source;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@SpringBootApplication
//@EnableEurekaClient
@MapperScan("com.hjh6.mp.multi.source.mapper")
public class HjhService2Application {
    public static void main(String[] args) {
        SpringApplication.run(HjhService2Application.class);
    }

}
