package com.hjh6.mp.multi.source.service.impl;

 import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjh6.mp.multi.source.entity.Dept;
import com.hjh6.mp.multi.source.entity.Menu;
import com.hjh6.mp.multi.source.mapper.DeptMapper;
import com.hjh6.mp.multi.source.service.IDeptService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
 public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

    @Override
      public List<Menu> findMenu() {

        List<Dept> deptById = this.baseMapper.findDeptById();
        for (Dept dept : deptById) {
            System.out.println("========>>dept:"+dept.getFullname());
        }
        List<Menu> menuList = this.baseMapper.findMenu();
        for (Menu menu : menuList) {
            System.out.println("========>menu:"+menu.getMenuName()+"|"+menu.getId()+"|"+menu.getMenuType());
        }
        return menuList;
    }



}
