package com.hjh6.mp.multi.source.mapper;

 import com.baomidou.dynamic.datasource.annotation.DS;
 import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hjh6.mp.multi.source.entity.Dept;
import com.hjh6.mp.multi.source.entity.Menu;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author panhao
 * @since 2017-07-11
 */
 @DS("master")
 public interface DeptMapper extends BaseMapper<Dept> {

    @DS("slave_1")
    @Select("select * from sys_menu")
    public List<Menu> findMenu();

    @Select("select * from sys_dept where id= 24")
    public List<Dept> findDeptById();
}