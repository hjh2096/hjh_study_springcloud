package com.hjh6.mp.multi.source.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hjh6.mp.multi.source.entity.Dept;
import com.hjh6.mp.multi.source.entity.Menu;

import java.util.List;

/**
 *
 *
 * @author panhao
 * @date 2017-04-27 17:00
 */
public interface IDeptService extends IService<Dept> {

  List<Menu> findMenu();
}
