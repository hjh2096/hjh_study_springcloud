package com.hjh.study.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class HjhService1Application {

    public static void main(String[] args) {
        SpringApplication.run(HjhService1Application.class);
    }
}
