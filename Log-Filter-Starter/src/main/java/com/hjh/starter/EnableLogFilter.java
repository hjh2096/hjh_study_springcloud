package com.hjh.starter;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(EnableLogFilterImportSelector.class)
// 可直接引入LogFilterAutoConfiguration配置类
// 通过引入ImportSelector来获取spring.factories中需要加载的自动配置类
public @interface EnableLogFilter {

}