package com.hjh.study.service2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hjh.study.service2.entity.Dept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author panhao
 * @since 2017-07-11
 */
public interface DeptMapper extends BaseMapper<Dept> {

}