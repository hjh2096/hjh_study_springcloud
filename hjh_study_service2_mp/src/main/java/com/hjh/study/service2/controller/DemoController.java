package com.hjh.study.service2.controller;


        import com.hjh.study.service2.entity.Dept;
        import com.hjh.study.service2.service.IDeptService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

        import java.util.List;

@RestController
@RequestMapping("/service2/demo")
public class DemoController {

    @Autowired
    private IDeptService deptService;
    @RequestMapping("/test1")
    public String test1(){
        return "service2|test1";
    }

    @RequestMapping("/test2")
    public String test2(){
        List<Dept> list = deptService.list();
        System.out.printf("=========>>list size:"+ list.size());
        return "service2|test1";
    }
}
