package com.hjh.study.service2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hjh.study.service2.entity.Dept;

/**
 * 部门服务
 *
 * @author panhao
 * @date 2017-04-27 17:00
 */
public interface IDeptService extends IService<Dept> {

}
