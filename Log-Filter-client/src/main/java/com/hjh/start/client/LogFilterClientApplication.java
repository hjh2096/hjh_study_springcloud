package com.hjh.start.client;

import com.hjh.starter.EnableLogFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication

@EnableEurekaClient
@EnableLogFilter
public class LogFilterClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(LogFilterClientApplication.class);
    }

}
